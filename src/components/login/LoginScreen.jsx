import { useContext } from "react";
import styled from "styled-components";
import { AuthContext } from "../../auth/AuthContext";
import { types } from "../../types/types";

const LoginScreen = ({ history }) => {
    const { dispatch } = useContext( AuthContext );

    const handleLogin = () => {

        const lastPatch = localStorage.getItem('lastPath') || '/';

        dispatch({
            type: types.login,
            payload: {
                name: 'Manuel',
            }
        });

        history.replace(lastPatch);
    }
 
    return (
        <Container>
            <h1>Login</h1>
            <hr />

            <Btn
                onClick={ handleLogin }
            >
                Login
            </Btn>
        </Container>
    );
}
 
export default LoginScreen;

const Container = styled.div`
    width: 85%;
    margin: 0 auto;
`;

const Btn = styled.div`
    background-color: #623CFA;
    border-radius: 4px;
    color: white;
    height: 40px;
    width: 80px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
`;