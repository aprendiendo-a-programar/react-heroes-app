import DcScreen from "./dc/DcScreen";
import MarvelScreen from "./marvel/MarvelScreen";
import NavBar from "./ui/NavBar";
import HeroScreen from "./heroes/HeroScreen";

export {
    DcScreen,
    MarvelScreen,
    NavBar,
    HeroScreen,
}