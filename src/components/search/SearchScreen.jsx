import queryString from 'query-string';
import { useMemo } from 'react';

import { useLocation } from "react-router";
import useForm from "../../hooks/useForm";
import HeroCard from "../heroes/HeroCard";
import styled from "styled-components";
import { getHeroesByName } from '../../selectors/getHeroesByName';

const SearchScreen = ({ history }) => {

    const location = useLocation();
    const { q = '' } = queryString.parse(location.search);

    const [formValues, handleInputChange] = useForm({
        searchText: q
    });

    const { searchText } = formValues;
    const heroesFiltered = useMemo(() => getHeroesByName(q), [q,]);

    const handleSearch = (e) => {
        e.preventDefault();
        history.push(`?q=${searchText}`)
    } 

    return (
        <Container>
            <h1>Search Screen</h1>
            <hr />

            <ContainerDivs>
                <SearchContainer>
                    <h3>Search Form</h3>

                    <Form onSubmit={handleSearch}>
                        <Input
                            type="text"
                            placeholder="Search..."
                            name="searchText"
                            value={searchText}
                            onChange={ handleInputChange }
                        />

                        <Button
                            type="submit"
                        >
                            Search
                        </Button>
                    </Form>
                </SearchContainer>

                <ResultConainer>
                    {
                        heroesFiltered.map((hero) => {
                            return (
                                <HeroCard
                                    key={hero.id}
                                    {...hero}
                                />
                            )
                        })
                    }
                </ResultConainer>
            </ContainerDivs>
        </Container>
    );
}

export default SearchScreen;

const Container = styled.section`

`;

const ContainerDivs = styled.div`
`;

const SearchContainer = styled.div`
    width: 100%;
`;

const ResultConainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    max-width: 100%;
    height: auto;
    gap: 10px;
    margin-top: 20px;
`;

const Form = styled.form`
    
`;

const Input = styled.input`
    width: 90%;
    margin-right: 10px;
`;

const Button = styled.button`

`;