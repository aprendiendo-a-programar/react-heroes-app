import React from 'react';
import { useContext } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import styled from 'styled-components'
import { AuthContext } from '../../auth/AuthContext';
import { types } from '../../types/types';


const Navbar = () => {
    const {user:{name}, dispatch} = useContext(AuthContext);
    const history = useHistory();
    
    const handleLogout = () => {
        dispatch({
            type: types.logout
        })

        history.replace('/login');
    }

    return (
        <Container>
            <ContainerSection>
                <ContainerNav>
                    <StyledLink
                        to="/"
                    >
                        Asociaciones
                    </StyledLink>

                    <StyledLink
                        activeClassName="active"
                        exact
                        to="/marvel"
                    >
                        Marvel
                    </StyledLink>

                    <StyledLink
                        activeClassName="active"
                        exact
                        to="/dc"
                    >
                        DC
                    </StyledLink>

                    <StyledLink
                        activeClassName="active"
                        exact
                        to="/search"
                    >
                        Buscar
                    </StyledLink>
                </ContainerNav>

                <ContainerLogin>

                    <NameUsr>{name}</NameUsr>

                    <BtnLogout
                        onClick={handleLogout}
                    >
                        Logout
                    </BtnLogout>
                </ContainerLogin>
            </ContainerSection>
        </Container>
    )
}

export default Navbar;

const Container = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: center;
    background-color: #1C1D1F;
    height: 70px;
`;

const ContainerSection = styled.section`
    width: 85%;
    margin: 0 auto;
    display: flex;
`;

const ContainerNav = styled.nav`
    display: flex;
    justify-content: space-between;
    gap: 15px;
    min-width: 40%;
    align-items: center;
`;

const ContainerLogin = styled.div`
    min-width: 45%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    gap: 10px;
`;

const StyledLink = styled(NavLink)`
    color: #fff;
`;

const NameUsr = styled.p`
    color: #7EF9FF;
`;

const BtnLogout = styled.button`
    background: none;
    border-radius: 4px;
    border: solid 1px #7EF9FF;
    color: #7EF9FF;
    height: 40px;
    width: 80px;
    font-weight: 600;
    cursor: pointer;
`;