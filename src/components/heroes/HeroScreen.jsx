import { useMemo } from "react";
import { Redirect, useParams } from "react-router";
import { getHeroById } from "../../selectors/getHeroById";
import styled from "styled-components";

const HeroScreen = ({ history }) => {
    const { heroeId } = useParams();
    const hero = useMemo(() => getHeroById( heroeId ), [heroeId]);

    if (!hero) {
        return <Redirect to="/" />;
    }

    const {
        superhero,
        publisher,
        alter_ego,
        first_appearance,
        characters,
    } = hero;

    const handleReturn = () => {
        if( history.length <= 2 ) {
            history.push('/')
        } else {
            history.goBack();
        }


    }

    return (
        <Container>
            <Img
                src={`../assets/heroes/${heroeId}.jpg`}
                alt={superhero}
                className="animate__animated animate__fadeInLeft"
            />
            <Info>
                <h1>{superhero}</h1>
                <p><b>Alter ego:</b> {alter_ego}</p>
                <hr />
                <p><b>Publisher:</b> {publisher}</p>
                <hr />
                <p><b>Firts appearance:</b> {first_appearance}</p>
                <hr />
                <p><b>Characters:</b> {characters}</p>
                <hr />

                <Btn onClick={ handleReturn }>Return</Btn>
            </Info>

        </Container>
    );
}

export default HeroScreen;

const Container = styled.section`
    margin-top: 40px;
    display: flex;
    gap: 10px 40px;

    @media screen and (max-width: 805px) {
        flex-wrap: wrap;
    }
`;

const Info = styled.article`
    width: 50%;

    @media screen and (max-width: 805px) {
        width: 100%;
    }
`;

const Img = styled.img`
    max-width: 50%;
    height: auto;

    @media screen and (max-width: 805px) {
        max-width: 100%;
    }
`;

const Btn = styled.button`
    border: 1px solid #623CFA;
    background-color: white;
    border-radius: 4px;
    color: #623CFA;
    height: 40px;
    width: 80px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;

    &:hover {
        border: 2px solid;
        font-weight: 600;
    }
`;