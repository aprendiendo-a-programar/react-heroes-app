import { Link } from "react-router-dom";
import styled from "styled-components";

const HeroCard = ({
    id,
    superhero,
    alter_ego,
    first_appearance
}) => {

    return (
        <Container>
            <Cards>
                <Img src={`./assets/heroes/${id}.jpg`} />
                <RowInfo>
                    <h5>{superhero}</h5>
                    <p>{ alter_ego }</p>
                    <p><small>{first_appearance}</small></p>
                    <Link to={`./hero/${id}`} >Ver más...</Link>
                </RowInfo>
            </Cards>
        </Container>
    );
}

export default HeroCard;

const Container = styled.section`
    width: 530px;
    border: 1px solid #BEBEBE;

    @media screen and (max-width: 1600px) {
        width: 400px;
    }
`;

const Cards = styled.div`
    display: flex;
    
    & > img {
        max-width: 100%;
        max-height: 300px;
    }
`;

const RowInfo = styled.article`
    padding: 10px;
`;

const Img = styled.img`

`;