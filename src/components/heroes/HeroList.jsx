import { getHeroesByPublisher } from "../../selectors/getHeroesByPublisher";
import HeroCard from "./HeroCard";
import styled from "styled-components";
import { useMemo } from "react";

const HeroList = ({ publisher }) => {
    const heroes = useMemo(() => getHeroesByPublisher( publisher ), [publisher]);

    return (
        <Container className="animate__animated animate__backInUp">
            {
                heroes.map((hero) => {
                    return(
                        <HeroCard 
                            key={hero.id}
                            { ...hero }
                        >
                        </HeroCard>
                    )
                })
            }
        </Container>
    );
}
 
export default HeroList;

const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    gap: 10px;
`;