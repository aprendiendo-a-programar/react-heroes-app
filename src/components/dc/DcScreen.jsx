import styled from "styled-components";
import HeroList from "../heroes/HeroList";

const DcScreen = () => {
    return (
        <Container>
            <h1>DC Screen</h1>
            <hr />
            <HeroList publisher={"DC Comics"} />
        </Container>
    );
}
 
export default DcScreen;

const Container = styled.div`

`;