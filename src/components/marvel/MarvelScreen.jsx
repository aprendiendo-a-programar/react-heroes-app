import styled from "styled-components";
import HeroList from "../heroes/HeroList";

const MarvelScreen = () => {
    return ( 
        <Container>
            <h1>Marvel Screen</h1>
            <hr />
            <HeroList publisher={"Marvel Comics"} />
        </Container>
    );
}
 
export default MarvelScreen;

const Container = styled.div`

`;