import React from 'react';
import { Route, Redirect } from 'react-router-dom';


const PrivateRoute = ({
    isAuthenticated,
    render: Component,
    ...rest
}) => {
    
    localStorage.setItem('lastPath', rest.location.pathname);

    return (
        <Route { ...rest }
            render={ (props) => (
                ( isAuthenticated )
                    ? ( <Component { ...props } /> )
                    : ( <Redirect to="/login" /> )
            )}
        
        />
    )
}

export default PrivateRoute;