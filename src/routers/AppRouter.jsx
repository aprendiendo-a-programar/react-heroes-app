import React from "react";
import { useContext } from "react";

import {
    BrowserRouter as Router,
    Switch
} from "react-router-dom";
import { AuthContext } from "../auth/AuthContext";
import LoginScreen from "../components/login/LoginScreen";
import DasboardRoutes from "./DashboardRoutes";
import PrivateRoute from "./PrivateRoute";
import PublicRoute from "./PublicRoute";

const AppRouter = () => {

    const { user } = useContext(AuthContext);

    return (
        <Router>
            <Switch>
                <PublicRoute 
                    exact path="/login" 
                    render={ LoginScreen }
                    isAuthenticated={ user.logged }
                />

                <PrivateRoute 
                    path="/"
                    render={ DasboardRoutes }
                    isAuthenticated={ user.logged }
                />
            </Switch>
        </Router>
    );
}

export default AppRouter;