import { Redirect, Route, Switch } from "react-router";
import { MarvelScreen, NavBar, HeroScreen, DcScreen } from "../components";
import styled from "styled-components";
import SearchScreen from "../components/search/SearchScreen";

const DasboardRoutes = ({ history }) => {
    return (
        <>
            <NavBar />

            <RouterContainer>
                <Switch>
                    <Route exact path="/marvel" component={MarvelScreen} />
                    <Route exact path="/dc" component={DcScreen} />
                    <Route exact path="/search" component={SearchScreen} />
                    <Route exact path="/hero/:heroeId" component={HeroScreen} />

                    <Redirect to="/marvel" />
                </Switch>
            </RouterContainer>
        </>
    );
}

export default DasboardRoutes;

const RouterContainer = styled.div`
    width: 85%;
    margin: 0 auto;
`;